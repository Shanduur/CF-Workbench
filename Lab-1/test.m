clear all; close all; clc;
s = tf('s');
k1 = 5/(1+3*s)^3;
k1 = k1 * 3/(s^2 + 1);
% u(t) = 1(t) <- heavyside function | h(t) <- step response
figure(1);
step(k1);
figure(2);
nyquist(k1);
% RMB -> show -> untick negative frequencies
% RMB -> Zoom on (-1,0)
% RMB -> Characteristics -> All Stability Margins
% RMB -> Properties
figure(3);
bode(k1);