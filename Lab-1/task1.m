clear all; close all; clc;
% OLS

% K(s) = k / (1 + ST)^3
% where k = 5 and T = 8
% Task 3 : k / (1 + ST)
% B = |k(jw*)|*A
k = 5;
T = 8;

s = tf('s');

k1 = k/(1+s*T)^2;

figure(1);
nyquist(k1);
title("Nyqusit of k/(1+s*T)^2");

figure(2)
bode(k1);
title("Bode of k/(1+s*T)^2");

figure(3)
step(k1)
title("Step response of k/(1+s*T)^2");

figure(4)
w = 2;
impulse(k1*w/(s^2 +  w^2));
hold on
impulse(w/(s^2 + w^2));
title("Impulse")