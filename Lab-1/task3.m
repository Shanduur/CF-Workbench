k1 = 5/(1+s*8)

k1 =
 
     5
  -------
  8 s + 1
 
Continuous-time transfer function.

kz = c2d(k1, 0.8, 'zoh')

kz =
 
    0.4758
  ----------
  z - 0.9048
 
Sample time: 0.8 seconds
Discrete-time transfer function.

clear kz
kd = c2d(k1, 0.8, 'zoh')

kd =
 
    0.4758
  ----------
  z - 0.9048
 
Sample time: 0.8 seconds
Discrete-time transfer function.

T = 8

T =

     8

Ti = T/10

Ti =

    0.8000

kd = c2d(k1, Ti, 'zoh')

kd =
 
    0.4758
  ----------
  z - 0.9048
 
Sample time: 0.8 seconds
Discrete-time transfer function.

step(kd)
stem(step(kd))
[y; t] = step(kd)
 [y; t] = step(kd)
  ?
Error: Multiple left-hand sides must be separated by commas.
 
[y, t] = step(kd)

y =

         0
    0.4758
    0.9063
    1.2959
    1.6484
    1.9673
    2.2559
    2.5171
    2.7534
    2.9672
    3.1606
    3.3356
    3.4940
    3.6373
    3.7670
    3.8843
    3.9905
    4.0866
    4.1735
    4.2522
    4.3233
    4.3877
    4.4460
    4.4987
    4.5464
    4.5896
    4.6286
    4.6640
    4.6959
    4.7249
    4.7511
    4.7748
    4.7962
    4.8156
    4.8331
    4.8490
    4.8634
    4.8764
    4.8881
    4.8988
    4.9084
    4.9171
    4.9250
    4.9322
    4.9386
    4.9445
    4.9497
    4.9545
    4.9589
    4.9628
    4.9663
    4.9695
    4.9724
    4.9750
    4.9774
    4.9796
    4.9815
    4.9833


t =

         0
    0.8000
    1.6000
    2.4000
    3.2000
    4.0000
    4.8000
    5.6000
    6.4000
    7.2000
    8.0000
    8.8000
    9.6000
   10.4000
   11.2000
   12.0000
   12.8000
   13.6000
   14.4000
   15.2000
   16.0000
   16.8000
   17.6000
   18.4000
   19.2000
   20.0000
   20.8000
   21.6000
   22.4000
   23.2000
   24.0000
   24.8000
   25.6000
   26.4000
   27.2000
   28.0000
   28.8000
   29.6000
   30.4000
   31.2000
   32.0000
   32.8000
   33.6000
   34.4000
   35.2000
   36.0000
   36.8000
   37.6000
   38.4000
   39.2000
   40.0000
   40.8000
   41.6000
   42.4000
   43.2000
   44.0000
   44.8000
   45.6000

stem(t, y)
rlocus(k1)
kcz = kz/1+kz
Unrecognized function or variable 'kz'.
 
kcz = kd/1+kd

kcz =
 
    0.9516 z - 0.8611
  ---------------------
  z^2 - 1.81 z + 0.8187
 
Sample time: 0.8 seconds
Discrete-time transfer function.

step(kd)
step(kcz)
step(kcz)
stem(step(kcz))
