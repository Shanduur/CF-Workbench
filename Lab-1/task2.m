s = tf('s')

s =
 
  s
 
Continuous-time transfer function.

k1 = 5/(1+3s)^3
 k1 = 5/(1+3s)^3
            ?
Error: Invalid expression. Check for missing multiplication operator, missing or unbalanced delimiters,
or other syntax error. To construct matrices, use brackets instead of parentheses.
 
k1 = 5/(1+3*s)^3

k1 =
 
              5
  -------------------------
  27 s^3 + 27 s^2 + 9 s + 1
 
Continuous-time transfer function.

rlocus(k1)
gain = 1.6

gain =

    1.6000

nyquist(k1)
figure(2)
rlocus(k1)
figure(3)
kr1 = 1

kr1 =

     1

kr2 = 0.8*gain

kr2 =

    1.2800

margin(k1)
k2 = kr2 * k1

k2 =
 
             6.4
  -------------------------
  27 s^3 + 27 s^2 + 9 s + 1
 
Continuous-time transfer function.

k2 = kr2 * k1 / (1 + kr2 * k1)

k2 =
 
                  172.8 s^3 + 172.8 s^2 + 57.6 s + 6.4
  --------------------------------------------------------------------
  729 s^6 + 1458 s^5 + 1215 s^4 + 712.8 s^3 + 307.8 s^2 + 75.6 s + 7.4
 
Continuous-time transfer function.

figure(4)
step(k2)
k2 = k1/(1+k1)

k2 =
 
                  135 s^3 + 135 s^2 + 45 s + 5
  ------------------------------------------------------------
  729 s^6 + 1458 s^5 + 1215 s^4 + 675 s^3 + 270 s^2 + 63 s + 6
 
Continuous-time transfer function.

figure(4)
step(k2)
figure(5)
kc = k1 * kr2 /( 1 + k1*kr2)

kc =
 
                  172.8 s^3 + 172.8 s^2 + 57.6 s + 6.4
  --------------------------------------------------------------------
  729 s^6 + 1458 s^5 + 1215 s^4 + 712.8 s^3 + 307.8 s^2 + 75.6 s + 7.4
 
Continuous-time transfer function.

step(kc)
figure(6)
margin(kc)
bode(k1)
bodemag(kc)